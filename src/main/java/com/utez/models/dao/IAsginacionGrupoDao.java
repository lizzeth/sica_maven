/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utez.models.dao;

import com.utez.models.entity.AsignacionGrupo;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Dell
 */
public interface IAsginacionGrupoDao extends JpaRepository<AsignacionGrupo,Serializable> {
    //public AsignacionGrupo findByProfesorId(Integer profesorId); 
    public List<AsignacionGrupo> findByProfesorId(Integer profesorId); 
}
