/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utez.models.dao;

import com.utez.models.entity.Profesor;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Dell
 */
public interface IProfesorDao extends JpaRepository<Profesor,Serializable>{
    public Profesor findByUsername(String username);
}
