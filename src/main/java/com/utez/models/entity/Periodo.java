/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utez.models.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author Dell
 */
@Entity
@Table(name = "periodo")
public class Periodo implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nombre;
    @JsonIgnore
    @OneToMany(mappedBy="periodo", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    private List<PeriodoHasGrupo> periodoHasGrupoList;
    @ManyToOne(fetch = FetchType.LAZY)
    private PeriodoYear periodoYear;

    public Periodo() {
        periodoHasGrupoList = new ArrayList<PeriodoHasGrupo>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<PeriodoHasGrupo> getPeriodoHasGrupoList() {
        return periodoHasGrupoList;
    }

    public void setPeriodoHasGrupoList(List<PeriodoHasGrupo> periodoHasGrupoList) {
        this.periodoHasGrupoList = periodoHasGrupoList;
    }

    public PeriodoYear getPeriodoYear() {
        return periodoYear;
    }

    public void setPeriodoYear(PeriodoYear periodoYear) {
        this.periodoYear = periodoYear;
    }
    
    private static final long serialVersionUID = 1L;
}
