/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utez.models.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author Dell
 */
@Entity
@Table(name = "alumno")
public class Alumno implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable=true)
    private String nombre;
    @Column(nullable=true)
    private String lastname;
    @Column(nullable=true)
    private String matricula;
    @JsonIgnore
    @OneToMany(mappedBy="alumno", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    private List<CalificacionAlumno> calificacionAlumnoList;
    @JsonIgnore
    @OneToMany(mappedBy="alumno", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    private List<AlumnoPeriodogrupo> alumnoPeriodogrupoList;

    public Alumno() {
        calificacionAlumnoList = new ArrayList<CalificacionAlumno>();
        alumnoPeriodogrupoList = new ArrayList<AlumnoPeriodogrupo>();
    }

    public Alumno(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public List<CalificacionAlumno> getCalificacionAlumnoList() {
        return calificacionAlumnoList;
    }

    public void setCalificacionAlumnoList(List<CalificacionAlumno> calificacionAlumnoList) {
        this.calificacionAlumnoList = calificacionAlumnoList;
    }

    public List<AlumnoPeriodogrupo> getAlumnoPeriodogrupoList() {
        return alumnoPeriodogrupoList;
    }

    public void setAlumnoPeriodogrupoList(List<AlumnoPeriodogrupo> alumnoPeriodogrupoList) {
        this.alumnoPeriodogrupoList = alumnoPeriodogrupoList;
    }
    
    private static final long serialVersionUID = 1L;
}
