/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utez.models.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Dell
 */
@Entity
@Table(name = "asignacion_grupo")
public class AsignacionGrupo implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String status;
    @JsonIgnore
    @OneToMany(mappedBy="asignacionGrupo", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    private List<CalificacionAlumno> calificacionAlumnoList;
    @ManyToOne(fetch = FetchType.LAZY)
    private Materia materia;
    @ManyToOne(fetch = FetchType.LAZY)
    private PeriodoHasGrupo periodoHasGrupo;
    @ManyToOne(fetch = FetchType.LAZY)
    private Profesor profesor;
    
    public AsignacionGrupo() {
        calificacionAlumnoList = new ArrayList<CalificacionAlumno>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<CalificacionAlumno> getCalificacionAlumnoList() {
        return calificacionAlumnoList;
    }

    public void setCalificacionAlumnoList(List<CalificacionAlumno> calificacionAlumnoList) {
        this.calificacionAlumnoList = calificacionAlumnoList;
    }

    public Materia getMateria() {
        return materia;
    }

    public void setMateria(Materia materia) {
        this.materia = materia;
    }

    public PeriodoHasGrupo getPeriodoHasGrupo() {
        return periodoHasGrupo;
    }

    public void setPeriodoHasGrupo(PeriodoHasGrupo periodoHasGrupo) {
        this.periodoHasGrupo = periodoHasGrupo;
    }

    public Profesor getProfesor() {
        return profesor;
    }

    public void setProfesor(Profesor profesor) {
        this.profesor = profesor;
    }
    
    private static final long serialVersionUID = 1L;
    
}
