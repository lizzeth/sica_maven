/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utez.models.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Dell
 */
@Entity
@Table(name = "periodo_has_grupo")
public class PeriodoHasGrupo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne(fetch = FetchType.LAZY)
    private Periodo periodo;
    @ManyToOne(fetch = FetchType.LAZY)
    private Grupo grupo;
    @JsonIgnore
    @OneToMany(mappedBy = "periodoHasGrupo", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<AlumnoPeriodogrupo> alumnoPeriodogrupoList;
    @OneToMany(mappedBy = "periodoHasGrupo", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    private List<AsignacionGrupo> asignacionGrupoList;

    public PeriodoHasGrupo() {
            alumnoPeriodogrupoList = new ArrayList<AlumnoPeriodogrupo>();
            asignacionGrupoList = new ArrayList<AsignacionGrupo>();
    }

    public PeriodoHasGrupo(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Grupo getGrupoId() {
        return grupo;
    }

    public void setGrupoId(Grupo grupoId) {
        this.grupo = grupoId;
    }

    public Periodo getPeriodoId() {
        return periodo;
    }

    public void setPeriodoId(Periodo periodoId) {
        this.periodo = periodoId;
    }

    public List<AlumnoPeriodogrupo> getAlumnoPeriodogrupoList() {
        return alumnoPeriodogrupoList;
    }

    public void setAlumnoPeriodogrupoList(List<AlumnoPeriodogrupo> alumnoPeriodogrupoList) {
        this.alumnoPeriodogrupoList = alumnoPeriodogrupoList;
    }

    public List<AsignacionGrupo> getAsignacionGrupoList() {
        return asignacionGrupoList;
    }

    public void setAsignacionGrupoList(List<AsignacionGrupo> asignacionGrupoList) {
        this.asignacionGrupoList = asignacionGrupoList;
    }
    private static final long serialVersionUID = 1L;

}
