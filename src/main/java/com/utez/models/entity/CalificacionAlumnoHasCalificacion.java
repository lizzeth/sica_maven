/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utez.models.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Dell
 */
@Entity
@Table(name = "calificacion_alumno_has_calificacion")
public class CalificacionAlumnoHasCalificacion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "calificacion_alumno_id")
    private int calificacionAlumnoId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "calificacion_id")
    private int calificacionId;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    public CalificacionAlumnoHasCalificacion() {
    }

    public CalificacionAlumnoHasCalificacion(Integer id) {
        this.id = id;
    }

    public CalificacionAlumnoHasCalificacion(Integer id, int calificacionAlumnoId, int calificacionId) {
        this.id = id;
        this.calificacionAlumnoId = calificacionAlumnoId;
        this.calificacionId = calificacionId;
    }

    public int getCalificacionAlumnoId() {
        return calificacionAlumnoId;
    }

    public void setCalificacionAlumnoId(int calificacionAlumnoId) {
        this.calificacionAlumnoId = calificacionAlumnoId;
    }

    public int getCalificacionId() {
        return calificacionId;
    }

    public void setCalificacionId(int calificacionId) {
        this.calificacionId = calificacionId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CalificacionAlumnoHasCalificacion)) {
            return false;
        }
        CalificacionAlumnoHasCalificacion other = (CalificacionAlumnoHasCalificacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.utez.models.entity.CalificacionAlumnoHasCalificacion[ id=" + id + " ]";
    }
    
}
