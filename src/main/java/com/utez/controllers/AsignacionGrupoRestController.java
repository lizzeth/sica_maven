/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utez.controllers;

import com.utez.models.entity.AsignacionGrupo;
import com.utez.service.IAsingacionGrupoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Dell
 */
@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class AsignacionGrupoRestController {

    @Autowired
    private IAsingacionGrupoService asginacionGrupoService;

    @GetMapping("/asignacionGrupo")
    public List<AsignacionGrupo> index() {
        return asginacionGrupoService.findAll();
    }

    @GetMapping("/asignacionGrupo/{id}")
    public AsignacionGrupo show(@PathVariable Integer id) {
        return asginacionGrupoService.findById(id);
    }
    
//    @GetMapping("/asignacionGrupoProfesor/{id}")
//    public AsignacionGrupo showProfesor(@PathVariable Integer id) {
//        return asginacionGrupoService.findByProfesorId(id);
//    }
    
     @GetMapping("/asignacionGrupoProfesor/{id}")
    public List<AsignacionGrupo> showProfesor(@PathVariable Integer id ) {
        return asginacionGrupoService.findByProfesorId(id);
    }
    
}
