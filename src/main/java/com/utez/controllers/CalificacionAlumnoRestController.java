/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utez.controllers;

import com.utez.models.entity.CalificacionAlumno;
import com.utez.service.ICalificacionAlumnoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Dell
 */
@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class CalificacionAlumnoRestController {

    @Autowired
    private ICalificacionAlumnoService calificacionAlumnoService;

    @GetMapping("/alumnos/{id}")
    public List<CalificacionAlumno> showAlumnos(@PathVariable Integer id) {
        return calificacionAlumnoService.findByAsignacionGrupoId(id);
    }

}
