package com.utez.security.payload.response;

public class MessageResponse {
	
	private String message;
	
	public MessageResponse(String message) {
		this.message = message;
	}
	
	public String getMessageResponse() {
		return this.message;
	}
	
}
