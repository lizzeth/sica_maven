package com.utez.security.payload.response;

import com.utez.models.entity.Profesor;

public class TokenResponse {
	
 	private String token;
 	private Profesor usuario;

    public TokenResponse(String token, Profesor usuario) {
        this.token = token;
        this.usuario = usuario;
    }

    public String getToken() {
        return this.token;
    }

	public Profesor getUsuario() {
		return usuario;
	}

	
    

}
