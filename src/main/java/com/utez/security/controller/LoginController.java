package com.utez.security.controller;

import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.utez.models.entity.Profesor;
import com.utez.security.exception.AuthenticationException;
import com.utez.security.exception.ValidationEntity;
import com.utez.security.exception.payload.ValidationError;
import com.utez.security.payload.response.MessageResponse;
import com.utez.security.payload.response.TokenResponse;
import com.utez.security.jwt.JwtTokenUtil;
import com.utez.service.IProfesorService;
import static org.hibernate.annotations.common.util.impl.LoggerFactory.logger;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
public class LoginController {

    private static final Log logger = LogFactory.getLog(Profesor.class);

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    IProfesorService usuarioService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserDetailsService userDetailsService;

    private MessageSource messageSource;

    @Autowired
    public LoginController(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody Profesor profesor) {

        authenticate(profesor.getUsername(), profesor.getPassword());

        UserDetails userDetails = userDetailsService.loadUserByUsername(profesor.getUsername());

        String token = jwtTokenUtil.generateToken(userDetails);

        Profesor user = usuarioService.findByUsername(profesor.getUsername());

        Profesor userResponse = new Profesor(user.getId(), user.getNombre(), user.getLastname(), user.getStatus(), user.getUsername(), user.getPassword());
        System.out.println(user.getNombre() + " " + user.getUsername());
        return ResponseEntity.status(HttpStatus.OK).body(new TokenResponse(token, userResponse));
    }

    @PostMapping("/usuario")
    public ResponseEntity<?> saveUsuario(@Valid @RequestBody Profesor profesor) {
        profesor.setPassword(bCryptPasswordEncoder.encode(profesor.getPassword()));
        logger.info("LLEGO AQUI =>" + profesor.toString());
        usuarioService.saveUsuario(profesor);

        return ResponseEntity.ok(profesor);

    }

//    @ExceptionHandler({TransactionException.class})
//    public ResponseEntity<String> handleAuthenticationException(TransactionException e) {
//    	
//		HttpStatus status = null;
//		
//		switch(e.getStatus()) {
//			case 500:
//				status = HttpStatus.INTERNAL_SERVER_ERROR;
//				break;
//			case 404:
//				status = HttpStatus.NOT_FOUND;
//				break;
//		}
//		
//        return ResponseEntity.status(status).body(e.getMessage());
//    }
    @ExceptionHandler({AuthenticationException.class})
    public ResponseEntity<?> handleAuthenticationException(AuthenticationException e) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new MessageResponse(e.getMessage()));
    }

    private void authenticate(String username, String password) {

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new AuthenticationException("Cuenta desabilitada", e);
        } catch (BadCredentialsException e) {
            throw new AuthenticationException("Usuario o contraseña incorrectos", e);
        }
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ValidationError processValidationError(MethodArgumentNotValidException ex) {

        ValidationEntity val = new ValidationEntity(messageSource);
        return val.processFieldErrors(ex);
    }

}
