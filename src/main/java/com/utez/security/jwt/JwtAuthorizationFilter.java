package com.utez.security.jwt;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.ExpiredJwtException;

import static com.utez.security.config.Constants.HEADER_AUTHORIZACION_KEY;;


public class JwtAuthorizationFilter extends OncePerRequestFilter{
	
	
	private UserDetailsService userDetailsService;
	private JwtTokenUtil jwtTokeUtil;
	
	public JwtAuthorizationFilter (JwtTokenUtil jwtTokeUtil,UserDetailsService userDetailsService) {
		this.jwtTokeUtil = jwtTokeUtil;
		this.userDetailsService = userDetailsService;
	}
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		String token = request.getHeader(HEADER_AUTHORIZACION_KEY);
		logger.debug("LLEGA EL TOKEN DEL HEADER===>"+ token);
		
		String username = null;
		 
		if(token != null ) {
			
			try {		
				username = this.jwtTokeUtil.getUsernameFromToken(token);
			} catch (IllegalArgumentException e) {
                logger.error("an error occured during getting username from token", e);
            } catch (ExpiredJwtException e) {
                logger.warn("the token is expired and not valid anymore", e);
            }
		} 

		
		if(username != null  && SecurityContextHolder.getContext().getAuthentication() == null) {
			
			UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);
			
			if(jwtTokeUtil.validateToken(token, userDetails)) {
				
				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				SecurityContextHolder.getContext().setAuthentication(authentication);
			}
			
		}
			
		
		filterChain.doFilter(request, response);
		
	}
	
	
}
