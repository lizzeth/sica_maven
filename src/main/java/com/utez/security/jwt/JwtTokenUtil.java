package com.utez.security.jwt;

import static com.utez.security.config.Constants.SUPER_SECRET_KEY;
import static com.utez.security.config.Constants.TOKEN_EXPIRATION_TIME;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenUtil {
	
	//private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public String getUsernameFromToken(String token) {
        
		String username;
        try {
            final Claims claims = getClaimsFromToken(token);
            username = claims.getSubject();
        } catch (Exception e) {
            username = null;
        }
        return username;
    }
	
	private Claims getClaimsFromToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(SUPER_SECRET_KEY)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            claims = null;
        }
        return claims;
    }
	
	public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("sub", userDetails.getUsername());
        claims.put("iat", new Date());
        return generateToken(claims);
    }
	
	String generateToken(Map<String, Object> claims) {
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(generateExpirationDate())
                .signWith(SignatureAlgorithm.HS512, SUPER_SECRET_KEY)
                .compact();
    }
	
	private Date generateExpirationDate() {
        return new Date(System.currentTimeMillis() + TOKEN_EXPIRATION_TIME * 1000);
    }
	
	public Date getExpirationDateFromToken(String token) {
        Date expiration;
        try {
            final Claims claims = getClaimsFromToken(token);
            expiration = claims.getExpiration();
        } catch (Exception e) {
            expiration = null;
        }
        return expiration;
    }
	
	private Boolean isTokenExpired(String token) {
        Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }
	
	public Boolean validateToken(String token, UserDetails userDetails) {
    	
    	String usernameDetails = userDetails.getUsername();
    	String usernameToken = getUsernameFromToken(token);
    	
    	return(usernameDetails.equals(usernameToken) && !isTokenExpired(token));
    }
}
