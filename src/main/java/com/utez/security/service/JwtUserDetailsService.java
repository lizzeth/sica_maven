package com.utez.security.service;

import com.utez.models.dao.IProfesorDao;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.utez.models.entity.Profesor;


@Service
public class JwtUserDetailsService implements UserDetailsService {
	
	@Autowired
	private IProfesorDao usuarioRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Profesor usuario = usuarioRepository.findByUsername(username);
		
		if(usuario != null) {
			return new User(usuario.getUsername(),usuario.getPassword(),mapToGrantedAuthorities("ROLE_PROFESOR") );
		}else {
			throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
		}
	}
	
    private List<GrantedAuthority> mapToGrantedAuthorities(Object authorities) {

            //Relacion ManytoMay User -> Rol_Usuario <- Rol
//        return authorities.stream() 
//                .map(authority -> new SimpleGrantedAuthority(authority.getName().name()))
//                .collect(Collectors.toList());

            List<GrantedAuthority> auths = new ArrayList<>();

            auths.add(new SimpleGrantedAuthority("ROLE_PROFESOR"));

            return auths;
}
	
	
//	public List<GrantedAuthority> buildgrante(int rol){
//		String[] roles = {"ROLE_PROFESOR","ROLE_ALUMNO","ROLE_ADMINISTRADOR"};
//		List<GrantedAuthority> auths = new ArrayList<>();
//		
//		for(int i = 0; i<rol; i++) {
//			auths.add(new SimpleGrantedAuthority(roles[i]));
//		}
//		
//		return auths;
//	}
	
}
