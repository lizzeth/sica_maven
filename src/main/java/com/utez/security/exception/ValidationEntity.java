package com.utez.security.exception;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import com.utez.security.exception.payload.ValidationError;


public class ValidationEntity {

	private MessageSource messageSource;
	
	
	
    @Autowired
    public ValidationEntity(MessageSource messageSource) {
        this.messageSource = messageSource;
    }
    
    
 
    public ValidationError processFieldErrors(MethodArgumentNotValidException ex) {
    	
    	BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
        
        ValidationError dto = new ValidationError();
 
        for (FieldError fieldError: fieldErrors) {
            String localizedErrorMessage = resolveLocalizedErrorMessage(fieldError);
            dto.addFieldError(fieldError.getField(), localizedErrorMessage);
        }
 
        return dto;
    }
    
    
    public String resolveLocalizedErrorMessage(FieldError fieldError) {
        Locale currentLocale =  LocaleContextHolder.getLocale();
        String localizedErrorMessage = messageSource.getMessage(fieldError, currentLocale);
 
        
 
        return localizedErrorMessage;
    }
    
}
