package com.utez.security.exception.payload;

import java.util.ArrayList;
import java.util.List;

public class ValidationError {
	
	private List<FieldError> fieldErrors = new ArrayList<>();
	
	private String status = "500";
	 
    public ValidationError() {
 
    }
 
    public void addFieldError(String path, String message) {
        FieldError error = new FieldError(path, message);
        fieldErrors.add(error);
    }

	public List<FieldError> getFieldErrors() {
		return fieldErrors;
	}

	public void setFieldErrors(List<FieldError> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
    
	
    
	    
}
