package com.utez.security.exception;

public class TransactionException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	private int status;
	
	public TransactionException(String message,int status) {
		super(message);
		this.status = status;
        
    }

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
}
