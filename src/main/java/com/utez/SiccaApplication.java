package com.utez;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SiccaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SiccaApplication.class, args);
	}
}
