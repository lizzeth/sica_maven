/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utez.service;

import com.utez.models.entity.Profesor;
import java.util.List;

/**
 *
 * @author Dell
 */
public interface IProfesorService {
    public void saveUsuario(Profesor profesor);
    
    public List<Profesor> findAll();

    public List<Profesor> findByProfesorId(Integer id);

    public Profesor findById(Integer id);
    
    public Profesor findByUsername(String username);
    
}
