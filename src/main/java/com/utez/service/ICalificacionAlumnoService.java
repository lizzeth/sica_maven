/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utez.service;

import com.utez.models.entity.CalificacionAlumno;
import java.util.List;

/**
 *
 * @author Dell
 */


public interface ICalificacionAlumnoService {
    
     public List<CalificacionAlumno> findByAsignacionGrupoId(Integer asignacionId);
}
