/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utez.service;

import com.utez.models.entity.AsignacionGrupo;
import com.utez.models.entity.Profesor;
import java.util.List;

/**
 *
 * @author Dell
 */
public interface IAsingacionGrupoService {

    public List<AsignacionGrupo> findAll();

    public List<AsignacionGrupo> findByProfesorId(Integer profesorId);

    public AsignacionGrupo findById(Integer id);

   // public AsignacionGrupo findByProfesorId(Integer profesorId);
}
