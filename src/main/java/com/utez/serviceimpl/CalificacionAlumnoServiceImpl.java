/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utez.serviceimpl;

import com.utez.models.dao.ICalificacionAlumnoDao;
import com.utez.models.entity.CalificacionAlumno;
import com.utez.service.ICalificacionAlumnoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Dell
 */
@Service
public class CalificacionAlumnoServiceImpl implements ICalificacionAlumnoService {
    
    @Autowired
    private ICalificacionAlumnoDao calificacionAlumnoDao;

    @Override
    public List<CalificacionAlumno> findByAsignacionGrupoId(Integer asignacionId) {
        return (List<CalificacionAlumno>) calificacionAlumnoDao.findByAsignacionGrupoId(asignacionId);
    }
    
    
}
