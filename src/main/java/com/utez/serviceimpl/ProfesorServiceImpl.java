/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utez.serviceimpl;

import com.utez.models.dao.IProfesorDao;
import com.utez.models.entity.Profesor;
import com.utez.service.IProfesorService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Dell
 */
@Service
public class ProfesorServiceImpl implements IProfesorService {
    
    @Autowired
    private IProfesorDao asignacionProfesorDao;

    @Override
    public void saveUsuario(Profesor profesor) {
        asignacionProfesorDao.save(profesor);
    }

    @Override
    public List<Profesor> findAll() {
        return (List<Profesor>) asignacionProfesorDao.findAll();
    }

    @Override
    public List<Profesor> findByProfesorId(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Profesor findById(Integer id) {
        return asignacionProfesorDao.findById(id).orElse(null);
    }

    @Override
    public Profesor findByUsername(String username) {
        return asignacionProfesorDao.findByUsername(username);
    }

   
}
