/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utez.serviceimpl;

import com.utez.models.dao.IAsginacionGrupoDao;
import com.utez.models.entity.AsignacionGrupo;
import com.utez.models.entity.Profesor;
import com.utez.service.IAsingacionGrupoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Dell
 */
@Service
public class AsignacionGrupoServiceImpl implements IAsingacionGrupoService {

    @Autowired
    private IAsginacionGrupoDao asginacionGrupoDao;

    @Override
    @Transactional(readOnly = true)
    public List<AsignacionGrupo> findAll() {
        return (List<AsignacionGrupo>) asginacionGrupoDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public AsignacionGrupo findById(Integer id) {
        return asginacionGrupoDao.findById(id).orElse(null);
    }

//    @Override
//    public AsignacionGrupo findByProfesorId(Integer profesorId) {
//        AsignacionGrupo unaAsignacion = asginacionGrupoDao.findByProfesorId(profesorId);
//        return unaAsignacion;
//    }

    @Override
    public List<AsignacionGrupo> findByProfesorId(Integer profesorId) {
       return (List<AsignacionGrupo>) asginacionGrupoDao.findByProfesorId(profesorId);
    }
 

    
}
